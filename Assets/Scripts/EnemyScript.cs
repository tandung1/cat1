using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    public float Speed;
    public GameObject Enemy;

    [SerializeField]
    private GameObject Bullet;

    [SerializeField]
    public GameObject Impact01;

    private float Boom = 0.5f;


    // Start is called before the first frame update
    void Start()
    {
        Enemy = gameObject;
        Speed = 25f;
        StartCoroutine(EnemyShoot());

    }

    // Update is called once per frame
    void Update()
    {
        Enemy.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -Speed * Time.deltaTime);
    }

    IEnumerator EnemyShoot()
    {
        yield return new WaitForSeconds(Random.Range(0f, 15f));
        Vector3 temp = transform.position;
        temp.y += -0.8f;

        Instantiate(Bullet, temp, Quaternion.identity);

        StartCoroutine(EnemyShoot());
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player1"))
        {
            StartCoroutine(EnemyExplosion());
        }
        else if (collision.gameObject.CompareTag("Player2"))
        {
            StartCoroutine(EnemyExplosion());
        }
    }



    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("BulletAlive"))
        {
            StartCoroutine(EnemyExplosion());
        }
    }

    IEnumerator EnemyExplosion()
    {
        yield return null;
        GameObject EnemyEx = Instantiate(Impact01, transform.position, Quaternion.identity) as GameObject;
        Destroy(Enemy);
        Destroy(EnemyEx, Boom);
    }
}
