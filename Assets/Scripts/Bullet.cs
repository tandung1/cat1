using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    float Speed = 2f;
    GameObject obj;

    BoxCollider2D Box;
    // Start is called before the first frame update
    void Start()
    {
        obj = gameObject;
        Speed = 2f;
        Vector3 Bounds = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0));
    }

    // Update is called once per frame
    void Update()
    {
        obj.GetComponent<Rigidbody2D>().velocity = new Vector2(0, Speed);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("EnemyShip"))
        {
            Object.Destroy(obj);
        } else if (collider.CompareTag("despawner")){
            Object.Destroy(obj);
        }
    }
}
