using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControllerScripts : MonoBehaviour
{
    public void Play2Player()
    {
        Debug.Log("@");
        SceneManager.LoadScene("Gameplay");
    }

    public void Play1Player()
    {
        SceneManager.LoadScene("Gameplay1");
    }
}
