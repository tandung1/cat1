using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControllScene1 : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private GameObject PanelPause;

    [SerializeField]
    private GameObject PanelDie;

    public static GameControllScene1 instance1;

    private void Awake()
    {
        MakeInstance1();
    }

    void MakeInstance1()
    {
        if (instance1 == null)
        {
            instance1 = this;
        }
    }

    public void PauseGame()
    {
        PanelPause.SetActive(true);
        Time.timeScale = 0;
    }

    public void ResumeButton()
    {
        PanelPause.SetActive(false);
        Time.timeScale = 1;
    }

    public void RestartButton()
    {
        SceneManager.LoadScene("Gameplay1");
        Time.timeScale = 1;
    }

    public void MenuButton()
    {
        SceneManager.LoadScene("menu");
        Time.timeScale = 1;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            PauseGame();
        }
    }

    public void DiedPanel()
    {
        PanelDie.SetActive(true);
        Time.timeScale = 0;
    }
}
