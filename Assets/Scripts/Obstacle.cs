using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public float Speed;
    private GameObject Rock;

    [SerializeField]
    public GameObject Impact01;

    private float Boom = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        Rock = gameObject;
        Speed = 20f;
    }

    // Update is called once per frame
    void Update()
    {
        Rock.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -Speed * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("BulletAlive"))
        {
            StartCoroutine(RockExplosion());
        } else if (collider.CompareTag("Player1"))
        {
            StartCoroutine(RockExplosion());
        }
        else if (collider.CompareTag("Player2"))
        {
            StartCoroutine(RockExplosion());
        }
    }

    IEnumerator RockExplosion()
    {
        yield return null;
        GameObject RockEx = Instantiate(Impact01, transform.position, Quaternion.identity) as GameObject;
        Destroy(Rock);
        Destroy(RockEx, Boom);
    }
}
